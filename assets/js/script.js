$(document).ready(function(){
	tablas();
	var url = unescape(window.location.href);
	var activate = url.split('/');
	var baseURL = activate[0]+'//'+activate[2]+'/'+activate[3]+'/';
	//var baseURL = activate[0]+'//'+activate[2]+'/';

	var $ipH = 0;
	// Meses
	var $mensual = 12;
	var $bimestral = 6;
	var $trimestral = 4;
	var $cuatrimestral = 3;
	var $semestral = 2;
	var $anual = 1;

	var $plazo = 0;
	var $periodicidad = 0;
	var $monto = 0;
	var $ano = 360;
	var $anoCompleto = 365;

	$("#eaU").keyup(function(){
		$eaU = parseFloat($("#eaU").val());
		$plazo = parseInt($("#plazo").val());
		
		if($("#semestral").is(':checked')){
			$selected = $semestral;
		}else if($("#trimestral").is(':checked')){
			$selected = $trimestral
		}else if($("#mensual").is(':checked')){
			$selected = $mensual;
		}else if($("#bimestral").is(':checked')){
			$selected = $bimestral;
		}else if($("#cuatrimestral").is(':checked')){
			$selected = $cuatrimestral;
		}else if($("#anual").is(':checked')){
			$selected = $anual;
		}

		$dias = ($ano / $selected);
		$pow = ($dias/360);
		$pow2 = (360/$dias);

		$ea = ($eaU / 100);
		$powT = Math.pow((1+$ea),$pow);

		$ip = ($powT)-1;
		$ipH = $ip;
		$naU = (($ip * $pow2) * 100).toFixed(2);
		$pvU = ($ip * 100).toFixed(2);

		$("#naU").val($naU);
		$("#pvU").val($pvU);
	});

	$("#naU").keyup(function(){
		$naU = parseFloat($("#naU").val());
		$plazo = parseInt($("#plazo").val());

		if($("#semestral").is(':checked')){
			$selected = $semestral;
		}else if($("#trimestral").is(':checked')){
			$selected = $trimestral
		}else if($("#mensual").is(':checked')){
			$selected = $mensual;
		}else if($("#bimestral").is(':checked')){
			$selected = $bimestral;
		}else if($("#cuatrimestral").is(':checked')){
			$selected = $cuatrimestral;
		}else if($("#anual").is(':checked')){
			$selected = $anual;
		}

		$dias = ($ano / $selected);
		$pow = ($dias/360);
		$pow2 = (360/$dias);

		$ip2 = (($naU / $plazo)/100);
		$ipH = $ip2;
		$eaU2 = ((Math.pow((1 + $ip2), $pow2))-1).toFixed(2);			
		$pvU2 = ($ip2 * 100).toFixed(2);

		$("#eaU").val($eaU2*100);
		$("#pvU").val($pvU2);
	});

	$("#pvU").keyup(function(){
		$pvU = parseFloat($("#pvU").val());
		$plazo = parseInt($("#plazo").val());

		if($("#semestral").is(':checked')){
			$selected = $semestral;
		}else if($("#trimestral").is(':checked')){
			$selected = $trimestral
		}else if($("#mensual").is(':checked')){
			$selected = $mensual;
		}else if($("#bimestral").is(':checked')){
			$selected = $bimestral;
		}else if($("#cuatrimestral").is(':checked')){
			$selected = $cuatrimestral;
		}else if($("#anual").is(':checked')){
			$selected = $anual;
		}

		$dias = ($ano / $selected);
		$pow = ($dias/360);
		$pow2 = (360/$dias);

		$ip3 = ($pvU / 100);
		$ipH = $ip3;
		$eaU3 = ((Math.pow((1 + $ip3), $pow2))-1).toFixed(2);
		$naU3 = (($ip3 * $pow2)*100).toFixed(2);

		$("#eaU").val($eaU3*100);
		$("#naU").val($naU3);
	});

	$("#simular").click(function(){
		if($("#semestral").is(':checked')){
			$plazo = parseFloat($plazo * $semestral);
		}else if($("#trimestral").is(':checked')){
			$plazo = parseFloat($plazo * $trimestral)
		}else if($("#mensual").is(':checked')){
			$plazo = parseFloat($plazo * $mensual);
		}else if($("#bimestral").is(':checked')){
			$plazo = parseFloat($plazo * $bimestral);
		}else if($("#cuatrimestral").is(':checked')){
			$plazo = parseFloat($plazo * $cuatrimestral);
		}else if($("#anual").is(':checked')){
			$plazo = parseFloat($plazo * $anual);
		}
		$monto = parseInt($("#monto").attr("data-value"));

		//console.log($ipH);
		//console.log($plazo);
		//console.log($monto);
		$cuotaFija = amortizacionCouta($monto, $ipH, $plazo);
		llenarTabla1($plazo, $monto, $cuotaFija, $ipH);
		$(this).attr("disabled", true);
	});

	$(document).on("click", ".seleccionarCuota", function (){
		$id = $(this).attr("id");
		$i = $(this).attr("data-i");
		$nombre = $(this).attr("data-nombre");
		$fecha = $(this).attr("data-fecha");

		$("#cuotaSelected").html($nombre);
		$("#cuotaSelected").attr("data-i", $i);
		$("#fechaSelected").html($fecha);

		goTop();
	});

	$("#simularMora").click(function(){
		$i = $("#cuotaSelected").attr("data-i");
		$i = parseInt($i);
		$seleccionarCuota = ($i - 1);
		$montoCuotaAnterior =  parseFloat($("#cuotaNumero"+$seleccionarCuota).attr("data-monto"));
		$cuotaFijaAnterior = parseFloat($("#cuotaNumero"+$seleccionarCuota).attr("data-CF"));
		console.log($i);
		console.log($seleccionarCuota);
		$monto = $montoCuotaAnterior;
		console.log($monto);
		$fecha = moment($("#fechaSelected").html());
		$fechaTabla = $("#fechaSelected").html();
		console.log($fecha);
		$fechaUsuario = moment($("#fechaMora").val());
		console.log($fechaUsuario.diff($fecha, 'days'), ' dias de diferencia');
		$("#diasMora").val($fechaUsuario.diff($fecha, 'days'));
		$dias = parseFloat($("#diasMora").val());
		$ea = parseFloat($("#usura").val());
		$ea = $ea / 100;
		$interesMora = interesMora($ea, $dias, $montoCuotaAnterior, $anoCompleto);
		$("#interesMora").val($interesMora.toFixed(2));
		$cuotaFija = $cuotaFijaAnterior + $interesMora;
		llenarTabla2($interesMora, $cuotaFija, $cuotaFijaAnterior, $fechaTabla);
		$("#interesMora").formatCurrency();
		$(this).attr("disabled", true);
	});

	//Helpers

	$("#id").blur(function(){
		var value = $(this).val();

		if(value > 0){
			$(this).attr("data-id", value);
			$(this).formatCurrency({roundToDecimalPlace: -1});
			$(this).toNumber({region: 'en-US'});
			//$(this).attr("disabled",true);
			$("#idpdf").html(value);
		}
	});

	$("#monto").blur(function(){
		var value = $(this).val();
		//value = (value/1000).toFixed(3);
		//console.log(value);
		if(value > 0){
			$(this).attr("data-value", value);
			$(this).formatCurrency();
			$(this).attr("disabled",true);

			$("#mspdf").html(value);
		}
	});

	$("#reinicarTablaCuotaFija").click(function(){
		$("#tbodyAmortCuoFij").html("");
	});

	$("#plazo").blur(function(){
		var value = $(this).val();
		if(value > 6){
			$(this).val(6);
		}else if(value <= 0){
			$(this).val(1);
		}
		
		$("#ppdf").html(value);
	});

	/*$("#pdf").click(function(){
		$monto = 1;
 		window.open(baseURL+'pdf/?id:'+$monto, '_blank');
	});*/

	$("#vMonto").click(function(){
		$("#monto").removeAttr("data-value");
		$("#monto").removeAttr("disabled");
		$("#monto").val("");
	});

	var doc = new jsPDF('l', 'pt', 'a4');

	var specialElementHandlers = {
	    '#editor': function (element, renderer) {
	        return true;
	    }
	};

	$('#pdf').click(function () {

		doc.fromHTML($('#datosPDF').html(), 50, 50, {
		    'width': 1920,
		    'margin': 1,
		    'elementHandlers': specialElementHandlers,
		    'pagesplit': true,
	  	}, function() {
	    	doc.save('spacesoftware.pdf');
	  	});
	});

	$("#nombre").blur(function(){
		$value = $(this).val();
		$("#nnpdf").html($value);
	});


	/*
	si es efectivo primero el periodico

		efecrivo anual 
		ip = (1+ea)^dias/360 * -1

		nominal anual 
		ip% * plazo
	

	si es nominal anual primero el periodico
		periodico vencid
		0o
		IP = interes nomial anual / # periodos 
		ea = (1+ip)^360/dias * -1

	Si es periodico vencido	
	se halla cualquiera




	Tabla de amortizacion cuota 

	# cuota 0 - 4 si es 2 años y semestral 2 * 2

	1. cuota fija y flujo de caja A = P ( (1 + ip)^n * ip / (1 + ip )^n -1)

	cuotas para amortizacion a intereses:
		1. monto * ip 

	amortizacion a capital 
		amortizacion a intereses - cuota fija
	
	saldo disponible 
		saldo a capital - amortizacion a capital




	mora 
		cuota a mora ... se selecciona
		fecha de pago 
		dias de mora = si tenia que pagar el 12 y pago el 20 , no se usa el 20 solo hasta 19, son 8 dias de mora
		tasa de usura = super intendenia

		**** Interes periodico = liquidar dias de mora = ip = (1 + ea )^dias / 365 * -1
		interes de mora = saldo a capital anterior a la cuota selecionada * el interes periodico de arriba ****
	*/
});

/**
 * Verifies if the string is in a valid email format
 * @param  {number} veces a llenar tabla
 * @return  {boolean}
*/
function llenarTabla1($veces, $monto, $cuotaFija, $ip){
	$("#tbodyAmortCuoFij>tr").remove(".odd");

	for($i = 0; $i <= $veces; $i++){
		var $fecha = new Date();
		$fecha = $fecha.getFullYear()+"-"+($fecha.getMonth()+(1+$i))+"-"+$fecha.getDate();
		if($i == 0){
			$("#tbodyAmortCuoFij").append("<tr> <td> <a class='seleccionarCuota' data-fecha='"+$fecha+"' data-monto='"+$monto+"' data-nombre='Número "+$i+"' id='cuotaNumero"+$i+"' >"+$i+"</a> </td> <td>"+$fecha+"</td> <td id=montoS"+$i+">"+$monto+"</td> <td>-</td> <td>-</td> <td>-</td> <td id=montoF"+$i+">"+$monto+"</td></tr>");
		}else{
			$intereses = amortizacionInteres($monto, $ip);
			$capital = $cuotaFija - $intereses;
			$monto = $monto - $capital;
			$("#tbodyAmortCuoFij").append("<tr> <td> <a class='seleccionarCuota' data-fecha='"+$fecha+"' data-i='"+$i+"' data-monto='"+$monto+"' data-CF='"+$cuotaFija+"' data-nombre='Número "+$i+"' id='cuotaNumero"+$i+"' >"+$i+"</a> </td> <td>"+$fecha+"</td> <td id=montoS"+$i+">"+$monto.toFixed(2)+"</td> <td id=montoC"+$i+">"+$cuotaFija.toFixed(2)+"</td> <td id=capital"+$i+">"+$capital.toFixed(2)+"</td> <td id=intereses"+$i+">"+$intereses.toFixed(2)+"</td> <td id=montoF"+$i+">"+$cuotaFija.toFixed(2)+"</td></tr>");
		}
		$("#capital"+$i).formatCurrency();
		$("#montoF"+$i).formatCurrency();
		$("#montoC"+$i).formatCurrency();
		$("#montoS"+$i).formatCurrency();
		$("#intereses"+$i).formatCurrency();
	}
	//paging("cuotaFija");
}

function llenarTabla2($interesMora, $cuotaFija, $cuotaFijaAnterior, $fecha){
	//var $fecha = new Date();
	//$fecha = $fecha.getFullYear() + "/" + ($fecha.getMonth()+1) + "/" + $fecha.getDate();

	for($i = 0; $i < 1; $i++){
		$("#tbodyAmortMora>tr").remove(".odd");
		$("#tbodyAmortMora").append("<tr> <td> <a class='seleccionarCuota' data-fecha='"+$fecha+"' data-nombre='Número "+$i+"' id='cuotaNumero"+$i+"' >"+$i+"</a> </td> <td>"+$fecha+"</td> <td id='cuotaFijaAnterior"+$i+"'>"+$cuotaFijaAnterior.toFixed(2)+"</td> <td id='interesMora"+$i+"''>"+$interesMora.toFixed(2)+"</td> <td id='flujoCaja"+$i+"'>"+$cuotaFija.toFixed(2)+"</td></tr>");
		
		$("#cuotaFijaAnterior"+$i).formatCurrency();
		$("#flujoCaja"+$i).formatCurrency();
		$("#interesMora"+$i).formatCurrency();
	}
	//paging("cuotaFija");
}

function paging(tabla){
	$("#"+tabla+"tbody").empty();
	$('#'+tabla).paging({limit:10});
}

function goTop(){
	$('html, body').animate({
        scrollTop: $("#moraDiv").offset().top
    }, 1000);
}

function amortizacionCouta($p, $ip, $n){
	var $a = 0;
	$pow = Math.pow((1 + $ip), $n);
	$pow2 = $pow - 1;
	$powxip = $pow * $ip;
	$powxip_pow2 = $powxip / $pow2;
	console.log($powxip_pow2);
	$a = $p * $powxip_pow2;
	return $a;
}

function amortizacionInteres($p, $ip){
	var $intereses = 0;
	$intereses = $p * $ip;
	return $intereses;
}

function amortizacionCoutaMora($p, $ip, $n){
	var $a = 0;
	$pow = Math.pow((1 + $ip), $n);
	$pow2 = $pow - 1;
	$powxip = $pow * $ip;
	$powxip_pow2 = $powxip / $pow2;
	console.log($powxip_pow2);
	$a = $p * $powxip_pow2;
	return $a;
}

function interesMora($ea, $dias, $montoCuotaAnterior, $anoCompleto){
	var $ip = 0;
	var $interes = 0;
	$powMora = ($dias / $anoCompleto); 
	$ip = (1 + $ea);
	$ip = Math.pow($ip, $powMora); 
	$ip = $ip - 1;
	$ip100 = $ip * 100;
	$("#ipMora").val($ip100.toFixed(2));
	$interes = $montoCuotaAnterior * $ip;
	return $interes;
}

function toDate(dateStr) {
  const [day, month, year] = dateStr.split("-")
  return new Date(year, month - 1, day)
}

function tablas() {
	if( typeof ($.fn.DataTable) === 'undefined'){ return; }

	var tablas = [];

	for(i = 0; i < tablas.length; i++){
		var handleDataTableButtons = function() {
		  if ($("#"+tablas[i]+"").length) {
			$("#"+tablas[i]+"").DataTable({
			  dom: "Bfrtip",
			  buttons: [
				{
				  	extend: "copy",
				  	className: "btn-sm",
				  	text: "Copiar"
				},
				{
				  	extend: "csv",
				  	className: "btn-sm",
				  	text: "Descargar en CSV"
				},
				{
				  	extend: "print",
				  	className: "btn-sm",
				  	text: "Imprimir Todo"
				},
				{
				  	extend: "excel",
				  	className: "btn-sm",
				  	text: "Excel"
				},
				{
				  	extend: "pdf",
				  	className: "btn-sm",
				  	text: "PDF"
				},
			  ],
			  "language": {
		            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
		       },
		       order: [[1, "desc"]],
			  responsive: true
			});
		  }
		};
		TableManageButtons = function() {
		  "use strict";
		  return {
			init: function() {
			  handleDataTableButtons();
			}
		  };
		}();
		TableManageButtons.init();
	}
}