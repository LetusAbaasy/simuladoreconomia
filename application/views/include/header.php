<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="refresh" content="7200" />
    <meta name="application-name" content="" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="revisit-after" content="30 days" />
    <meta name="distribution" content="web" />
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW" />
    <!--<link href="<?php echo base_url('assets/img/favicon16.png')?>" type="image/png" sizes="16x16" rel="icon" />
    <link href="<?php echo base_url('assets/img/favicon32.png')?>" type="image/png" sizes="32x32" rel="icon" />
    <link href="<?php echo base_url('assets/img/favicon64.png')?>" type="image/png" sizes="64x64" rel="icon" />
    <link href="<?php echo base_url('assets/img/favicon128.png')?>" type="image/png" sizes="128x128" rel="shortcut icon" />-->
    <meta name="theme-color" content="#000000"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="white-translucent" />
    <!-- Title -->
	<title>SpaceSoftware Simulador | <?php echo $title; ?></title>
</head>
<body class="nav-md" id="datosPDF">
    <!--<div class="se-pre-con"></div>-->
    <header id="header">
        <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/img/logo.png'); ?>" alt="Logo"></a>
            </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <!--<li><a href="<?php echo base_url('home'); ?>">Home</a></li>-->
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </header>