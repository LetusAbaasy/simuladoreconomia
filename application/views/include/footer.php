	<footer>
		<p>Copyright 2018 - SpaceSoftware <a href="https://get.adobe.com/es/flashplayer/" target="_blank">Flash Player</a></p>
	</footer>
	<div class="hidden" id="assets">
	    <!-- Styles -->
	    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
	    <!-- Custom CSS -->
	    <link href="<?php echo base_url('assets/css/styles.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/notifIt.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/bootstrap-select.min.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/animate.min.css')?>" rel="stylesheet" type="text/css" />
	    <link href="<?php echo base_url('assets/css/bootstrap-dropdownhover.min.css')?>" rel="stylesheet" type="text/css" />
	    <!-- Scripts -->
	    <script src="<?php echo base_url('assets/js/modernizr.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/jquery-3.1.1.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/notifIt.js')?>" type="text/javascript" ></script>
	    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/jquery.validate.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/bootstrap-select.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/langs/selector-i18n/defaults-es_ES.js')?>" type="text/javascript"></script>
	        <!-- Data Tables -->
	    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/dataTables.buttons.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/dataTables.bootstrap.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.html5.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.print.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.flash.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.colVis.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/jszip.min.js')?>" type="text/javascript"></script>
	    <!--<script src="<?php echo base_url('assets/js/pdfmake.min.js')?>" type="text/javascript"></script>-->
	    <script src="<?php echo base_url('assets/js/vfs_fonts.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/buttons.bootstrap.min.js')?>" type="text/javascript"></script>
	        <!-- Fin Data Tables -->  
	    <script src="<?php echo base_url('assets/js/sidebar-menu.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/jquery.formatCurrency-1.4.0.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/i18n/jquery.formatCurrency.es-CO.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/paging.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/bootstrap-dropdownhover.min.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/echarts.min.js')?>"></script>
	    <script src="<?php echo base_url('assets/js/map/js/world.js')?>"></script>
	   	<!--<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>-->
	    <script src="<?php echo base_url('assets/js/jspdf.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/plugins/addhtml.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/html2canvas.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/moment.min.js')?>" type="text/javascript"></script>
	    <script src="<?php echo base_url('assets/js/js.js')?>" type="text/javascript"></script>
	</div>
</body>
<div id="editor"></div>
</html>