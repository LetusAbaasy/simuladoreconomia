<div class="container-fluid">
    <hr/>
    <hr/>
    <h2>Información:</h2>
    <hr/>
    <hr/>
    <div class="row-fluid">
        <div class="col-md-6">
            <div class="form-group">
                <label for="nombre">Nombre del cliente:* <span id="nnpdf"></span></label>
                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre...">
            </div>
            <div class="form-group">
                <label for="id">Identificacion:* <span id="idpdf"></span></label>
                <input type="text" class="form-control" name="id" id="id" placeholder="Identificacion...">
            </div>
            <div class="form-group">
                <label for="">Monto:* <span id="mspdf"></span></label>
                <div class="form-group">
                  <input type="text" id="monto" class="form-control" placeholder="Monto..." aria-describedby="basic-addon1" value="">
                  <a id="vMonto">Volver a ingresar monto</a>
                </div>
            </div>
            <div class="form-group">
                <label for="plazo">Plazo:* <span id="ppdf"></span><small>Años</small></label>
                <input type="number" class="form-control" name="plazo" max="6" min="1" id="plazo" placeholder="Plazo..." value="">
            </div>
            <div class="form-group">
                <label for="">Periodicidad:*</label>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="mensual">
                    <label class="form-check-label" for="mensual">
                        Mensual
                    </label>
                </div>
                <div class="form-check hidden">
                    <input class="form-check-input" type="checkbox" value="" id="bimestral">
                    <label class="form-check-label" for="bimestral">
                        Bimestral
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="trimestral">
                    <label class="form-check-label" for="trimestral">
                        Trimestral
                    </label>
                </div>
                <div class="form-check hidden">
                    <input class="form-check-input" type="checkbox" value="" id="cuatrimestral">
                    <label class="form-check-label" for="cuatrimestral">
                        Cuatrimestral
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="semestral" checked>
                    <label class="form-check-label" for="semestral">
                        Semestral
                    </label>
                </div>
                <div class="form-check hidden">
                    <input class="form-check-input" type="checkbox" value="" id="anual">
                    <label class="form-check-label" for="anual">
                        Anual
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="">Tasa de interess:*</label>
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon3">Efectivo anual:</span>
                  <input type="number" class="form-control" id="eaU" aria-describedby="basic-addon3">
                  <span class="input-group-addon" id="basic-addon3">%</span>
                </div>
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon3">Nominal Anual:</span>
                  <input type="number" class="form-control" id="naU" aria-describedby="basic-addon3">
                  <span class="input-group-addon" id="basic-addon3">%</span>
                </div>
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon3">Periodico vencido:</span>
                  <input type="number" class="form-control" id="pvU" aria-describedby="basic-addon3">
                  <span class="input-group-addon" id="basic-addon3">%</span>
                </div>
            </div>
            <button id="simular" class="btn btn-success btn-block">Calcular</button>
        </div>
        <div id="moraDiv" class="col-md-6">
            <h1>Mora</h1> 
            <h4>Cuota seleccionada: <label id="cuotaSelected">-</label> - <label id="fechaSelected">-</label></h4>
            <div class="form-group">
                <label for="fechaMora">Dias de mora:*</label>
                <input type="date" class="form-control" name="fechaMora" id="fechaMora" value='<?php echo date('Y-m-d');?>' placeholder="Dias de mora...">
            </div>
            <div class="form-group">
                <label for="diasMora">Dias de mora:*</label>
                <input type="number" class="form-control" name="diasMora" id="diasMora" disabled placeholder="Dias de mora...">
            </div>
            <div class="form-group">
                <label for="usura">Tasa de usura:*</label>
                <input type="number" class="form-control" name="usura" id="usura" placeholder="Tasa..." value="30.66" disabled>
                <small>Referencia https://www.dinero.com/pais/articulo/tasa-de-usura-para-mayo-en-colombia/257995</small>
            </div>
            <div class="form-group">
                <label for="ipMora">Interes periodico:*</label>
                <input type="number" class="form-control" name="ipMora" id="ipMora" placeholder="Interes periodico..." disabled>
            </div>
            <div class="form-group">
                <label for="interesMora">Interes mora:*</label>
                <input type="text" class="form-control" name="interesMora" id="interesMora" placeholder="Intereses mora..." disabled>
            </div>
            <button id="simularMora" class="btn btn-success btn-block">Calcular Mora</button>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <h3>Exportar en PDF</h3>
        <button class="btn btn-danger btn-block" id="pdf">PDF</button>
        <hr/>
        <hr/>
        <div class="col-md-12">
            <h1>Tabla de amortizacion cuota fija</h1><a id="reinicarTablaCuotaFija"><small>Reiniciar</small></a>
            <table id="cuotaFija" width="100%" border=0 class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td><label># Cuota</label></td>
                        <td><label>Fecha</label></td>
                        <td><label>Saldo a capital</label></td>
                        <td><label>Cuota Fija</label></td>
                        <td><label>Amortizacion a capital</label></td>
                        <td><label>Amortizacion Intereses</label></td>
                        <td><label>Flujo de caja</label></td>
                    </tr>
                </thead>
                <tbody id="tbodyAmortCuoFij"></tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <div class="col-md-12">
            <h1>Tabla de amortizacion cuota fija mora</h1>
            <table id="tablaMora" width="100%" border=0 class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td><label># Cuota</label></td>
                        <td><label>Fecha</label></td>
                        <td><label>Cuota Fija anterior</label></td>
                        <td><label>Interes mora</label></td>
                        <td><label>Flujo caja</label></td>
                    </tr>
                </thead>
                <tbody id="tbodyAmortMora"></tbody>
            </table>
        </div>
    </div>
</div>
<hr/>